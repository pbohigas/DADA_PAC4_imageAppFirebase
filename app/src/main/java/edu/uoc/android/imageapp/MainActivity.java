package edu.uoc.android.imageapp;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import edu.uoc.android.imageapp.utils.FileUtils;
import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // Request code
    private final int REQUEST_CODE_CAMERA = 1;
    private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
    private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;
    // Views
    private Button buttonOpenImage;
    private ImageView imageView;
    private TextView tvMessage;
    // Folder path
    private String folderPath;
    // Firebase
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Firebase
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, this.getClass().getSimpleName(), null);

        // Set views
        buttonOpenImage = findViewById(R.id.image_app_btn_capture);
        imageView = findViewById(R.id.image_app_iv_picture);
        tvMessage = findViewById(R.id.image_app_tv_message);

        // Set listeners
        buttonOpenImage.setOnClickListener(this);

        // Set Folder Path
        folderPath = Environment.getExternalStorageDirectory() + File.separator + FileUtils.FOLDER_IMAGE_PATH_NAME;

        // Set visibility depending if file exists
        setViewsVisibility();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // If device hasn't have external storage, the option menus won't be visible
        if (FileUtils.isExternalStorageAvailable()) {
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_save).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            onDeleteMenuTap();
            return true;
        } else if (item.getItemId() == R.id.action_save) {
            onSaveMenuTap();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onDeleteMenuTap() {
        // check permissions
        if (!hasPermissionsToWrite()) {
            // request permissions
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                              REQUEST_PERMISSION_STORAGE_DELETE);
        } else if (existAnyImage()) {
            showDeleteDialog();
        }
    }

    private void onSaveMenuTap() {
        // check permissions
        if (!hasPermissionsToWrite()) {
            // request permissions
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                              REQUEST_PERMISSION_STORAGE_SAVE);
        } else if (imageIsDisplayed()) {
            saveImage();
        }
    }

    private boolean hasPermissionsToWrite() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // getting the image from camera app
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CAMERA) {
            Bundle extras = data.getExtras();
            imageView.setImageBitmap((Bitmap) extras.get("data"));
            imageView.setVisibility(View.VISIBLE);
            tvMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == buttonOpenImage) {

            // send pressed button details Firebase
            Bundle params = new Bundle();
            params.putInt("ButtonID", v.getId());
            mFirebaseAnalytics.logEvent("Launch_camera_app", params);

            // launching an intent to get an image from camera app
            launchSystemCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_STORAGE_DELETE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // show dialog if image file exists
                    if (existAnyImage()) {
                        showDeleteDialog();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    showMessage(R.string.no_storage_permission_message);
                }
            }
            case REQUEST_PERMISSION_STORAGE_SAVE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // save the image file
                    saveImage();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    showMessage(R.string.no_storage_permission_message);
                }
            }
        }
    }

    private void launchSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_CODE_CAMERA);
        } else {
            showMessage(R.string.no_system_camera_app_message);
        }
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Add the buttons
        builder.setPositiveButton(R.string.dialog_delete_button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //** Send event to Firebase **/
                final Bundle params = new Bundle();
                params.putInt("ButtonID", id);
                mFirebaseAnalytics.logEvent("Delete_photo", params);

                // User clicked OK button
                FileUtils.deleteFile(folderPath, FileUtils.IMAGE_FILE_NAME);
                showMessage(R.string.image_delete_message);
                setViewsVisibility();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.dialog_delete_button_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.cancel();
            }
        });
        // Set other dialog properties
        builder.setTitle(R.string.dialog_delete_title);
        builder.setMessage(R.string.dialog_delete_message);

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void saveImage() {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        FileUtils.createImageFile(folderPath, FileUtils.IMAGE_FILE_NAME, bitmap);
        showMessage(R.string.image_save_message);
        setViewsVisibility();
    }

    private void setViewsVisibility() {
        if (FileUtils.existFile(folderPath, FileUtils.IMAGE_FILE_NAME)) {
            // Set visibility
            imageView.setVisibility(View.VISIBLE);
            tvMessage.setVisibility(View.GONE);
            // Set image
            File imageFile = FileUtils.getFile(folderPath, FileUtils.IMAGE_FILE_NAME);
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setVisibility(View.GONE);
            tvMessage.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(null);
        }
    }

    private void showMessage(@StringRes int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }

    private boolean existAnyImage() {
        return FileUtils.existFile(folderPath, FileUtils.IMAGE_FILE_NAME) || imageIsDisplayed();
    }

    private boolean imageIsDisplayed() {
        return imageView.getVisibility() == View.VISIBLE;
    }
}
